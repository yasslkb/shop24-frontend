import {BACKEND_STAGE_URL} from "./Staging";
import {BACKEND_PRODUCTION_URL} from "./Production";
import {isProductionMode} from "./AppConfig";

let url;

url = isProductionMode ? BACKEND_PRODUCTION_URL : BACKEND_STAGE_URL;

export const BACKEND_URL = url;