import {BACKEND_URL} from './environment/AppEnvironment'

export const apiRoutes = {
    getListProducts: BACKEND_URL + '/products',
    getProductDetail: BACKEND_URL + '/products/',
  };

  export const pageRoutes = {
    main: '/',
    detail: '/product/:id',
    carts: '/carts'
  };
  