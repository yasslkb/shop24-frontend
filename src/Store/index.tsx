import { createStore } from 'redux';
import ShopAppReducer from "../reducers/index";


const store =  createStore(ShopAppReducer);

export default store;