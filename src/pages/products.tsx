import React from 'react';
import  ProductsList from '../components/ProductList/ProductsList';

const Products = () => {
  return (
    <>
      <ProductsList/>
    </>
  );
};

export default Products;
