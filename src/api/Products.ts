import {
    useFetch
  } from '../utils/reactQuery';
  import { apiRoutes } from '../routes';
  import {
    ProductInfoInterface
  } from '../interfaces/ProductInterface';
  import { pathToUrl } from '../utils/router';
  
  export const useGetProductsList = () =>
    useFetch<ProductInfoInterface[]>(apiRoutes.getListProducts);
  
  export const useGetProduct = (id: number) =>
    useFetch<ProductInfoInterface>(apiRoutes.getProductDetail+id);