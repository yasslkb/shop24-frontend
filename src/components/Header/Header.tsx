import { render } from "react-dom"
import { Link } from 'react-router-dom';
import CardStateInterface from "../../interfaces/redux-interfaces/CardStateInterface";
import { connect,ConnectedProps } from "react-redux";
import './header.scss';


const Header : React.FC<CardHeaderProps>= ({numberItems}) => {





    return(
        
        <div className="header-container">
        <div>
            <nav className="navbar">
                  <ul>
                      <li><Link to="/" >Products</Link></li>
                      <li><Link to="/carts">Carts : <span className="items-Count">{numberItems}</span></Link></li>
                  </ul>
            </nav>
        </div>
      </div>


        );
};



const mapStateToProps = (state:any)=>{
    return{
        numberItems:state.cardReducer.numberItems
};
};


const connector =connect(mapStateToProps,{});


type CardHeaderProps = ConnectedProps<typeof connector>;

export default connector(Header);


