import React, { useRef } from 'react';
import { useGetProduct } from '../../api/Products';
import { useParams } from 'react-router-dom';

import ProductDetail from './ProductDetail';

const ProductsDetail = () => {
   
  const { id } = useParams<{ id: string }>();
  console.log("id: ",id);
    const {
      data: product,
      isLoading
    } = useGetProduct(parseInt(id?id:'0'));



    return (

        <div>
    
            {isLoading? (

                <span>Loading...</span>
            ):(
            <div className='product-list'>
            {
                
              <React.Fragment key={product?.id || 0}>
                
                  <ProductDetail
                    id={product?.id}
                    image={product?.image}
                    rating={product?.rating}
                    description={product?.description}
                    category={product?.category}
                    price={product?.price}
                    title={product?.title}
                  />
               
              </React.Fragment>
              }
          </div>
            )
            }

        </div>


    );


}
export default ProductsDetail;