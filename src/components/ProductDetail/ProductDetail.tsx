import './ProductsDetail.scss'

type Props = {
    category?: string
    description?: string
    id?: number
    image?: string
    price?: number
    rating?: { rate?: number, count?: number }
    title?: string
  };

const ProductDetail = ({id,image,price,rating,title,category,description}:Props) =>{

return(

                <div className="product-detail" key={id}>
                   <div>
                    <img className="product-detail__image" alt="" src={image}/>
                    </div>
                    
                    <h2  className="product-detail__title">{title}</h2 >
                    <p className="product-detail__price">{`PRICE: ${price} $`}</p>
                    <p>{category}</p>
                    <p>{description}</p>
                    <div className="product-detail__rating">
                            <span>{`Rating: ${rating?.rate}`}</span>
                            <span className="product-detail__rating__count">{`(${rating?.count})`}</span>
                    </div>
                </div>



);

};


export default ProductDetail;