import React from 'react';
import { connect,ConnectedProps} from 'react-redux';
import CardStateInterface from '../../interfaces/redux-interfaces/CardStateInterface';
import { IncreaseQuantity , DecreaseQuantity,DeleteCart} from '../../actions';
import CartItemInterface from '../../interfaces/redux-interfaces/CardItemInterface'
import { Dispatch } from 'redux';
import './cards.scss';

const CardList:React.FC<CardListProps> = ({products,increaseQuantity,decreaseQuantity,deleteCart}) =>
{
 
    let ListCart : CartItemInterface[]=[];
    let TotalCart=0;


    products.map((item:any) => {
        if (item?.quantity && item?.price) {
        TotalCart+=item!!.quantity * item!!.price;
        ListCart.push(item);
        }
    });

    function TotalPrice(price:number,tonggia:number){
        return Number(price * tonggia).toLocaleString('en-US');
    }

    return (
    <>

<div>
  <div>
    <table className="cart-table">
      <thead>
        <tr>
          <th></th>
          <th>Name</th>
          <th>Image</th>
          <th>Price</th>
          <th>Quantity</th>
          <th>Total Price</th>
        </tr>
      </thead>
      <tbody>
        {ListCart.map((item: CartItemInterface, key: number) => {
          return (
            <tr key={key}>
              <td>
                <i className="delete-button" onClick={() => deleteCart({ id: item.id })}>
                  X
                </i>
              </td>
              <td>{item.title}</td>
              <td>
                <img src={item.image} alt="Product" />
              </td>
              <td>{item.price} $</td>
              <td>
                <div className="quantity-cell">
                  <span onClick={() => decreaseQuantity({ id: item.id })}>-</span>
                  <span>{item.quantity}</span>
                  <span onClick={() => increaseQuantity({ id: item.id })}>+</span>
                </div>
              </td>
              <td>{TotalPrice(item.price, item.quantity)} $</td>
            </tr>
          );
        })}

        <br/>
        <br/>
        <br/>
        <br/>
        <tr className="total-carts-row">
          <td>Total Carts</td>
          <td>{Number(TotalCart).toLocaleString('en-US')} $</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

    </>
  );
};


const mapStateToProps = (state:any) =>{
   
    console.log("state", state);
      return{
          products:state.cardReducer.products
      };
  };


  const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
     increaseQuantity: (item:object) => dispatch(IncreaseQuantity(item)),
     decreaseQuantity: (item:object) => dispatch(DecreaseQuantity(item)),
     deleteCart: (item:object) => dispatch(DeleteCart(item)),
    };
  };


const connector =connect(mapStateToProps,mapDispatchToProps);

type CardListProps = ConnectedProps<typeof connector>;

export default connector (CardList);