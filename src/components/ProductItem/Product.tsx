import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { pathToUrl } from '../../utils/router';
import { pageRoutes } from '../../routes';
import './ProductItem.scss';

type Props = {
    category: string
    description: string
    id: number
    image: string
    price: number
    rating: { rate: number, count: number }
    title: string,
    addCart:Function
  };



const ProductItem = ({id,image,price,rating,title,addCart}:Props) =>{
return(

        <div className="product-item" key={id}>
    <div>
     <img className="product-item__image" alt="" src={image}/>
     </div>
     
     <h2  className="product-item__title">{title}</h2 >
     <p className="product-item__price">{`PRICE: ${price} $`}</p>
     <div className="product-item__rating">
             <span>{`Rating: ${rating.rate}`}</span>
             <span className="product-item__rating__count">{`(${rating.count})`}</span>
     </div>

     <Link
          to={pathToUrl(pageRoutes.detail, { id })}
          style={{ color: 'inherit' }}
        >
            Details! 
        </Link> 

        <button className="product-item__addTocart-button" onClick={() => addCart({id,image,price,title})}>Add to cart!</button>

        
        </div>

                  
);
};

export default ProductItem;