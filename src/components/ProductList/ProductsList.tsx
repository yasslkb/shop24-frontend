import React from 'react';
import ProductItem from '../ProductItem/Product';
import { useGetProductsList } from '../../api/Products';
import './ProductsList.scss';
import { connect, ConnectedProps } from 'react-redux';
import CardStateInterface from '../../interfaces/redux-interfaces/CardStateInterface';
import { AddCart } from '../../actions';
import { Dispatch } from 'redux';
import CartItemInterface from '../../interfaces/redux-interfaces/CardItemInterface';

type ProductsListProps = ConnectedProps<typeof connector>;

const ProductsList: React.FC<ProductsListProps> = ({ addCart }) => {
  const { data: list, isLoading } = useGetProductsList();

  return (
    <div>
      {isLoading ? (
        <span>Loading...</span>
      ) : (
        <div className="product-list">
          {list!.map((product) => (
            <React.Fragment key={product.id || 0}>
              <ProductItem
                id={product.id}
                image={product.image}
                rating={product.rating}
                description={product.description}
                category={product.category}
                price={product.price}
                title={product.title}
                addCart={addCart}
              />
            </React.Fragment>
          ))}
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state: CardStateInterface) => ({});

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    addCart: (item:CartItemInterface) => dispatch(AddCart(item)),
  };
};

const connector = connect(mapStateToProps, mapDispatchToProps);

export default connector(ProductsList);
