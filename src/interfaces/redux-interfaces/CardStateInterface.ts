import CartItemInterface from "./CardItemInterface";


export default interface CardStateInterface {
    numberItems : number,
    products : CartItemInterface[]
}