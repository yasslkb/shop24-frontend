import { ProductInfoInterface } from "../ProductInterface"
import CartItemInterface from "./CardItemInterface"
export default interface ActionTypeInterface{
        type:String,
        payload:CartItemInterface 
}