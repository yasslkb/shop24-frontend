import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router } from 'react-router-dom';
import './index.css';
import App from './App';
import { QueryClient, QueryClientProvider } from "react-query";
import { Provider } from 'react-redux';
import store from './Store';

const queryClient = new QueryClient();

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <Router>
    <QueryClientProvider client={queryClient}>
      <Provider store={store}>
         <App/>
      </Provider>
     
    </QueryClientProvider>
    </Router>
   
  </React.StrictMode>
);
