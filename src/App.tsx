import React, { Fragment } from 'react';
import './App.css';
import { Route, Routes} from 'react-router-dom';
import { pageRoutes } from './routes';
import Products from './pages/products';
import ProductDetail from './pages/ProductDetail';
import Header from './components/Header/Header';
import Card from './pages/card';



function App() {
  return (

    <div className="App">

      <Header/>
      <Routes>
        
        <Route path={pageRoutes.main} element={
          <Products/>
        }>
        </Route>

        <Route path={pageRoutes.detail} element={<ProductDetail/>}>
        </Route>
        <Route path={pageRoutes.carts} element={<Card/>}>
        </Route>
      </Routes>

    </div>

  );
}

export default App;
