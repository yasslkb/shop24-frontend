import { combineReducers } from 'redux';
import {GET_NUMBER_CART,ADD_CART, DECREASE_QUANTITY, INCREASE_QUANTITY, DELETE_CART} from '../actions'
import CardStateInterface from '../interfaces/redux-interfaces/CardStateInterface'
import ActionTypeInterface from '../interfaces/redux-interfaces/ActionTypeInterface';

const initialState: CardStateInterface = {
    numberItems :0,
    products :[]
}

const cardReducer = (state:CardStateInterface=initialState,action:ActionTypeInterface) => {

    switch(action.type){
        case GET_NUMBER_CART:
                return{
                    ...state
                }
        case ADD_CART:
            if(state.numberItems==0){
                let item = {
                    id:action.payload.id,
                    quantity:1,
                    title:action.payload.title,
                    image:action.payload.image,
                    price:action.payload.price
                } 
                state.products.push(item); 
            }
            else{
                let check = false;
                state.products.map((item,key)=>{
                    if(item?.id==action.payload.id){
                        state.products[key]!!.quantity++;
                        check=true;
                    }
                });
                if(!check){
                    let _item = {
                        id:action.payload.id,
                        quantity:1,
                        title:action.payload.title,
                        image:action.payload.image,
                        price:action.payload.price
                    }
                    state.products.push(_item);
                }
            }
            return{
                ...state,
                numberItems:state.numberItems+1
            }
            case INCREASE_QUANTITY:
                const updatedProducts = state.products.map((product) => {
                    console.log(action)
                    console.log("id matched", product.id);
                    if (product.id === action.payload.id) {
                      return {
                        ...product,
                        quantity: product.quantity + 1
                      };
                    }
                    return product;
                  });
              
               return{
                   ...state,
                   numberItems: state.numberItems + 1,
                   products:updatedProducts
               }
            case DECREASE_QUANTITY:
                const updatedProductsDecrease = state.products.map((product) => {
                    if (product.id === action.payload.id && product.quantity > 1) {
                      return {
                        ...product,
                        quantity: product.quantity - 1
                      };
                    }
                    return product;
                  });
              
                  return{
                    ...state,
                    numberItems: state.numberItems > 1 ? state.numberItems - 1 : state.numberItems,
                    products:updatedProductsDecrease
                }
                case DELETE_CART:
                    const deletedProduct = state.products.find((product) => product.id === action.payload.id);
                    const deletedQuantity = deletedProduct?.quantity ?? 0;
                  
                    const updatedNumberItems = state.numberItems - deletedQuantity;
                    const updatedProductsDelete = state.products.filter((item) => item?.id !== action.payload.id);
                  
                    return {
                      ...state,
                      numberItems: updatedNumberItems,
                      products: updatedProductsDelete
                    };
                  
        default:
            return state;
    }
}


const ShopAppReducer = combineReducers({
        cardReducer
});

export default ShopAppReducer;