describe('Add item to the cart', () => {

  beforeEach(() => {
     cy.visit('/'); 
  });


  it('should add the first item to the cart!', () => { 
    
    cy.get('.product-item')
    .first()
    .find('.product-item__addTocart-button')
    .click();

    cy.get('.items-Count')
    .should('have.text','1')
    .parent()
    .click();

    cy.get('tbody')
    .find('tr')
    .should('have.length',2);
  })

})
